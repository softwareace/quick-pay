define(function (require, exports, module) {
	require('jquery');
	var Vue = require('vue');
	var common = require('common');
	require('compContdown');
	require('qrcode');
	new Vue({
		el: '#main-container',
		data: {
			payData: {},
			countdownData: {},
			showWeixinMobile: false,
			showAlipayMobile: false,
			showWeixinAndroid: false,
			showWeixinIOS: false,
			showWixinPC: false,
			showAlipayPC: false,
			isWeixin: false,
			isAlipay: false,
			payResultTimer: null,
			isExpired: false,
			qrcodeUrl: ''
		},
		mounted: function () {

		},
		methods: {
			initHandler: function (initData) {
				this.payData = initData;
				this.qrcodeUrl = this.payData.code_url;
				this.createQrcode(this.payData.code_url);
				var payType = this.payData.pay_type || 'WXPAY';
				this.showWeixinMobile = payType == 'WXPAY' && common.isMobile();
				this.showAlipayMobile = payType == 'ALIPAY' && common.isMobile();
				this.showWeixinAndroid = payType == 'WXPAY' && common.isAndroid();
				this.showWeixinIOS = payType == 'WXPAY' && common.isIOS();
				this.showWixinPC = !common.isMobile() && payType == 'WXPAY';
				this.showAlipayPC = !common.isMobile() && payType == 'ALIPAY';
				this.isWeixin = payType == 'WXPAY';
				this.isAlipay = payType == 'ALIPAY';
				if (this.showAlipayMobile) common.open(this.payData.code_url);
			},
			createQrcode: function (url) {
				var qrcode = new QRCode(this.$refs.show_qrcode_container, {
					width: 210,
					height: 210
				});

				qrcode.makeCode(url);
			},

			countDownEventHandle: function (data) {
				this.countdownData = data;
			},
			countDownStart: function () {

			},
			countDownEnd: function () {
				clearInterval(this.payResultTimer);
				this.isExpired = true;
			},
			getInitData: function (initData) {
				this.initHandler(initData);
				this.payResultTimer = setInterval(function () {
					var params = {
						orderid: common.getUrlParams().orderid
					};
					common.req('getPayResult', params, function (res) {
						if (res.code == 0) {
							if (res.data.pay == 1) {
								clearInterval(this.payResultTimer);
								common.redirect(res.data.url);
							}
						}
					}.bind(this));
				}, 2e3);
			},
			downloadQrcode: function(){
				if(this.showWeixinMobile){
					common.redirect('http://mobile.qq.com/qrcode?url=' + this.qrcodeUrl);
				}
			}
		}
	});
});